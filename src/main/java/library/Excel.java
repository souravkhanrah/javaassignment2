package library;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {
	String xlFilePath="D:\\Excel Files Selenium\\SwasLab.xlsx";
	XSSFWorkbook wb;
	XSSFSheet sheet;
	public Excel() {
	    try {
			FileInputStream fis=new FileInputStream(new File(this.xlFilePath));
			System.out.println("File input Stream is created successfully!!");
			wb=new XSSFWorkbook(fis);
			sheet=wb.getSheetAt(0);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getUserName() {
		String cell1=wb.getSheetAt(0).getRow(1).getCell(0).getStringCellValue();
		System.out.println("username="+cell1);
		return cell1;
	}
	public String getPassword() {
		String cell2=wb.getSheetAt(0).getRow(1).getCell(1).getStringCellValue();
		System.out.println("password="+cell2);
		return cell2;
	}
	
//public  void readSheetData() {
//		Iterator<Row> rows=sheet.iterator();
//		// to fetch first col value and second col value from excel sheet
//		XSSFCell cell1=wb.getSheetAt(0).getRow(1).getCell(0);
//		System.out.println(cell1);
//		XSSFCell cell2=wb.getSheetAt(0).getRow(1).getCell(1);
//		System.out.println(cell2);
//		
//		while(rows.hasNext()) {
//			Row currRow=rows.next();
//			Iterator<Cell> cells=currRow.cellIterator();
//			
//			while(cells.hasNext()) {
//				Cell currCell=cells.next();
//				//if the password is integer then we have to convert string to int
//				CellType ctype=currCell.getCellType();
//				String value="";
//				if(ctype==CellType.STRING) {
//					value=currCell.getStringCellValue();
//				}else if(ctype==CellType.NUMERIC){
//					value=value+currCell.getNumericCellValue();
//				}
//				
//			//	String value=currCell.getStringCellValue();
//				System.out.println("Value for cell:-"+value);
//			}
//		}
	//}
//	public static void main(String[] args) {
//		Excel xl=new Excel();
//		xl.readSheetData();
//	}
//	
	
}
