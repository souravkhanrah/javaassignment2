package app.pages;

public class ProductsPage {
	//Sauce Labs Backpack
	static String addTocartXP="//div[text()='##LABEL##']/ancestor::div[1]/following-sibling::div/button";
	public static String getAddToCartXP(String productTitle) {
		return addTocartXP.replace("##LABEL##",productTitle);
	}
	//for selecting drop down
	public static String getDropdownXP() {
		return "//select[@class='product_sort_container']";
	}
	//for Add to cart icon
	public static String getAddToCartIconXP() {
		return "//a[@class='shopping_cart_link']";
	}
}
