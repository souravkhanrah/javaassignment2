package app.pages;

public class YourInformationPage {
public static String getFirstNameID() {
	return "first-name";
}
public static String getLastNameID() {
	return "last-name";
}
public static String getZipId() {
	return "postal-code";
}
public static String continueBtnID() {
	return "continue";
}

}
